/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica.jsp;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author mm-mobile
 */
public class LoginBean {
    
    private String login;
    private String senha;
    private String perfil;
    private final GregorianCalendar validade = new GregorianCalendar();
    
    private int hora = validade.get(Calendar.HOUR);

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

   
    
    
}
